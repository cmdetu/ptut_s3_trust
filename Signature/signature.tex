\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}

\begin{document}

\section{Signature RSA SHA-256}

\textit{Le chiffrement RSA est utilisé dans cette partie, une description de cet algorithme est présente en annexe de ce dossier.}

\url{https://tools.ietf.org/html/rfc8017}

\subsection{Introduction}
Par analogie à la signature manuscrite, la signature numérique permet d'identifier une personne. L'objectif de ce procédé est de garantir l'identité de l'émetteur d'un message mais aussi d'assurer, si la signature est vérifiée, que les données n'ont pas été modifiées. Cependant, elle n'est pas physiquement associée au document signé, il convient donc de trouver un moyen de lier logiquement ces deux éléments. Pour ce faire, nous allons utiliser deux algorithmes :

\begin{itemize}
    \item Un algorithme de hachage
    \item Un algorithme de chiffrement asymétrique
\end{itemize}

Dans le cadre de DNSSEC, l'association de RSA et SHA-256 pour la signature peut être utilisée (\href{https://tools.ietf.org/html/rfc5702}{RFC 5702}). \\

La combinaison d'un algorithme de hachage et d'un algorithme de chiffrement asymétrique permet d'assurer les propriétés suivantes.

\begin{itemize}
    \item authenticité (assure de la provenance)
    \item intégrité (les données n'ont pas été modifiées)
    \item non-répudiation (une signature ne peut pas être niée par son auteur)
\end{itemize}

Nous allons dans ce document nous concentrer sur le schéma de signature RSASSA-PKCS1-v1\_5. \\

\textit{Notons ce schéma est déterministe (un même message signé avec une même clé donne toujours la même signature) mais que RSASSA-PSS ne l'est pas.}

\subsection{Prérequis}
Tout au long de la signature et de la vérification, nous aurons besoins de plusieurs fonctions que nous allons décrire ici. \\

\subsubsection{Encodage}
Cette fonction prend un paramètre noté \(emLen\) qui doit vérifier la propriété suivant : \(emLen \geq tLen + 11\) avec \(tLen\) la taille de \(T\) que nous définissons plus bas, dans le cas de SHA-256, cette taille est de 51. \\
Si la propriété n'est pas vérifiée, la fonction se termine. \\

Le résultat de cet encodage est le suivant :
\[0x00 || 0x01 || PS || 0x00 || T\]
avec \(||\) l'opérateur de concaténation. \\

\(PS\) est formé de \((emLen - tLen - 3)\) octets de valeur \(0xff\). Notons que cela représente au moins 8 octets :
\[emLen \geq tLen + 11\]
\[emLen - tLen \geq 11\]
\[emLen - tLen - 3 \geq 8\]

T, quant à lui est le résultat de la concaténation des valeurs
\[0x30, 0x31, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04\]
\[0x02, 0x01, 0x05, 0x00, 0x04, 0x20\]
et du résultat de la fonction de hachage (dans notre cas il se code sur 256 bits donc 32 octets, d'où la valeur \(tLen = 19 + 32 = 51\)). \\

La fonction d'encodage renvoie \(0x00 || 0x01 || PS || 0x00 || T\) qui est codé sur \(emLen\) octets. \\
\begin{itemize}
    \item 3 octets pour \(0x00\), \(0x00\) et \(0x01\)
    \item \(emLen - tLen - 3\) octets pour \(PS\)
    \item \(tLen\) octets pour \(T\)
\end{itemize}
\[3 + emLen - tLen - 3 + tLen = emLen\]

\paragraph{Exemple : }
Cherchons le résultat de l'encodage pour le fichier rfc8017.txt avec \(emLen = 64\) (correspond à une clé RSA tel que \(n\) est codé sur 512 bits, les clés utilisées en pratique sont bien plus grandes). \\

\(PS\) est de taille \(emLen - tLen - 3 = 64 - 51 - 3 = 10\), ce qui correspond à 10 octets de valeur \(0xff\). \\

Le haché du fichier est le suivant : \[1e72dc473d18df3fc5598cdc12795a9f18f36f1aef15abc23a55eb0d58151d11\]

Par simple concaténation, voici le résultat (en hexadécimal) :
\[00 \; 01 \; ff \; ff \; ff \; ff \; ff \; ff \; ff \; ff \; ff \; ff \; 00\]
\[30 \; 31 \; 30 \; 0d \; 06 \; 09 \; 60 \; 86 \; 48 \; 01 \; 65 \; 03 \; 04\]
\[02 \; 01 \; 05 \; 00 \; 04 \; 20 \; 1e \; 72 \; dc \; 47 \; 3d \; 18 \; df\] \[3f \; c5 \; 59 \; 8c \; dc \; 12 \; 79 \; 5a \; 9f \; 18 \; f3 \; 6f \; 1a\] \[ef \; 15 \; ab \; c2 \; 3a \; 55 \; eb \; 0d \; 58 \; 15 \; 1d \; 11\]

\subsubsection{D'une suite d'octets à un entier}
RSA ne permet de chiffrer que des entiers. C'est pour cette raison que nous devons maintenant convertir le résultat de l'encodage en un entier. Cette fonction de conversion est nommée OS2IP (Octet Stream to Integer Primitive). Les octets passés en paramètre sont considérés comme les coefficients de la décomposition d'un nombre en base 256 (un octet code une valeur entre 0 et 255). \\

Notons \(x_i\) le \(i^{eme}\) octet (en commençant avec \(i = 0\) (\(0 \leq i \leq emLen - 1\)) tel que \(x_0\) représentera l'élément de poids fort). X est la valeur à calculer.

\[X = x_0 \times 256^{emLen - 1} + x_1 \times 256^{emLen - 2} + ... + x_{emLen - 2} \times 256^{1} + x_{emLen - 1} \times 256^{0}\]

Prenons un exemple simple (cet exemple ne peut pas se produire en pratique du fait de la faible valeur de \(emLen\)). Si le résultat de l'encodage est la suite d'octets suivante : \(0x01 \; 0x02 \; 0x02\), alors on a \(X = 1 \times 256^2 + 2 \times 256^1 + 2 \times 256^0 = 65536 + 512 + 2 = 66050\). \\

Avec une suite de \(n > 0\) octets, le traitement opéré est le suivant :

\(X_0 = 0\). Pour chaque octet \(o_i\) avec \(0 \leq i \leq n - 1\) de la suite on a
\[X_{i + 1} = X_{i} \times 256 + o_i\]
On calcule les éléments jusqu'à \(X_n\). \\

Pour la suite \(0x01 \; 0x02 \; 0x02\), cela correspond à :
\(X_0 = 0\)
\[X_1 = X_0 \times 256 + 1 = 1\]
\[X_2 = X_1 \times 256 + 2 = 256 + 2 = 258\]
\[X_3 = X_2 \times 256 + 2 = 258 \times 256 + 2 = 66050\]
Le résultat renvoyé est \(X_3 = 66050\).

\subsubsection{D'un entier à une suite d'octets}
La signature finale est une suite d'octets, nous aurons donc besoin d'en calculer une à partir d'un entier. \\
La fonction utilisée pour cette dernière conversion est nommée I2OSP (Integer to Octet Stream Primitive). \\

On opère par divisions successives par 256 jusqu'à ce que le quotient devienne nul. \\

Prenon par exemple \(X\) qui se décompose ainsi :

\[X = A \times 256^n + B \times 256^{n - 1} + ... + C \times 256 + D\]

On cherche les coefficients \(A, B, C, D...\) qui sont des octets (valeur entre 0 et 255).

\[X = 256 \times (A \times 256^{n - 1} + B \times 256^{n - 2} + ... + C) + \mathbf{D}\]

\[A \times 256^{n - 1} + B \times 256^{n - 2} + ... + C = 256 \times (A \times 256^{n - 2} + B \times 256^{n - 3} + ...) + \mathbf{C}\]

\[...\]

\[A \times 256 + B = 256 \times (A) + \mathbf{B}\]

\[A = 256 \times 0 + \mathbf{A}\]

Ce sont les restes des divisions qui représentent les valeurs des octets. \\

\paragraph{Exemple}
Prenons le même exemple que précédemment. \\

Le paramètre est \(X = 66050\).
\[66050 = 258 \times 256 + \mathbf{2}\]
\[258 = 1 \times 256 + \mathbf{2}\]
\[1 = 0 \times 256 + \mathbf{1}\]

On obtient donc la suite d'octets suivante : \(0x01 \; 0x02 \; 0x 02\) Qui est bien le résultat escompté.




\subsection{Calcul de la signature}

Voici l'algorithme de haut niveau qui correspond au calcul de la signature.

\begin{itemize}
    \item Encoder le message pour obtenir une suite de \(k\) octets (avec \(k\) la taille en octets du module de chiffrement \(n\) de la clé RSA utilisée pour signer).
    
    \item Convertir le résultat obtenu en un entier (OS2IP).
    
    \item Chiffrer cet entier avec la clé privée.
    
    \item Convertir le résultat du chiffrement en une suite de \(k\) octets (I2OSP) qui est la signature.
\end{itemize}
    
\paragraph{Remarque}

La première étape peut être à l'origine d'une erreur si la fonction de hachage n'accepte pas des données de la taille de celles fournies (pour SHA-256, la taille maximale est de plus de 2 millions de Terra-octets) ou si la valeur de \(emLen\) est trop faible (\(emLen < tLen + 11\)). Si une erreur se produit, arrêter l'exécution. \\

Notons que la fonction d'encodage assure que le résultat obtenu pourra être chiffré avec la clé fournie. En effet, dans l'algorithme de signature, le paramètre \(emLen\) correspond à la taille en octets de \(n\), le module de chiffrement. Le premier octet du résultat de la fonction d'encodage contient la valeur \(0x00\) et le second est \(0x01\), de ce fait, une fois la conversion effectuée, nous traitons un nombre sur exactement \(emLen - 1\) octets, qui ne posera pas de problème au chiffrement par RSA.


\subsection{Vérification de la signature}

Le destinataire ayant à présent reçu les données ainsi que leur signature, il peut procéder à la vérification. Pour cela, le processus est symétrique à celui de la signature. \\

Voici l'algorithme de haut niveau : \\

\begin{itemize}

    \item La taille de la signature reçue doit correspondre à la taille en octets du module de chiffrement \(n\) de la clé RSA. Sinon, renvoyer faux.
    
    \item Convertir la signature en un entier (OS2IP).
    
    \item Déchiffrer le résultat obtenu avec la clé publique de l'émetteur supposé.
    
    \item Convertir le résultat en une suite \(S1\) de \(k\) octets (avec \(k\) la taille en octets du module de chiffrement \(n\)) (I2OSP).
    
    \item Encoder le message reçu pour obtenir une suite \(S2\) de \(k\) octets.
    
    \item Si les octets de \(S1\) et \(S2\) sont identiques, renvoyer "vrai", cela signifie que le message n'a pas été modifié et provient bien du propriétaire de la clé privée associée à la clé publique utilisée pour la vérification (ou du moins d'un utilisateur qui possède la clé privée). Sinon, les données ont peut être été modifiées ou l'expéditeur n'est pas l'utilisateur supposé.
\end{itemize}

\paragraph{Remarque }
Plusieurs erreurs peuvent survenir lors de l'exécution de cet algorithme. Voici les fonctions qui peuvent être à l'origine de ces erreurs :

\begin{itemize}
    \item La fonction de déchiffrement RSA si la valeur à traiter est supérieure ou égale à \(n\).
    
    \item La fonction I2OSP si la valeur à convertir est trop grande par rapport à la taille de résultat demandée en paramètre.
    
    \item La fonction d'encodage si \(emLen\) est trop faible ou si la fonction de hachage ne peut pas traiter cette taille de données.
\end{itemize}

Si l'une de ces situations se produit, cela signifie que la signature est incorrecte. Il faut donc renvoyer "faux". \\

\end{document}

