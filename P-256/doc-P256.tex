\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{url}

\begin{document}

\section{Question de la courbe P-256}

\subsection{Introduction}

Les courbes elliptiques utilisées en cryptographie sont nombreuses. Dans les années 1990, diverses propositions ont été lancées et reprises par les organismes de normalisation, en particulier le NIST (National Institute of Standards and Technology). 
\newline
\newline
Dans FIPS 186 (Normes fédérales de traitement de l'information), une quinzaine de “courbes standards”  ont été décrites; la plus célèbre d'entre elles est la courbe P-256.

\subsection{La courbe P-256}

La courbe NIST P-256 est une courbe dite de Weierstrass (forme simplifiée de l’équation d’une courbe elliptique) . La génération et la validation de signature à l’aide de cette courbe sont effectuées grâce à l’algorithme de signature numérique à courbe elliptique (ECDSA).  \newline

\begin{figure}[!h]
    \centering
    \includegraphics[scale=0.5]{../ressources/Graph_stat_DNSSEC.png}
    \caption{Statistique DNSSEC pour .com en 2017 }
\end{figure}

Comme le montre la figure 1, P-256 est la courbe elliptique la plus populaire dans DNSSEC en juin 2017.
Cependant, Ed25519 a été normalisé pour une utilisation dans DNSSEC par RFC8080 en février 2017, suite à des échos relevant d'éventuels problèmes de sécurité avec la courbe P-256.
\newline
\newline
Cela vient ainsi remettre en cause la fiabilité de la courbe P-256 dans la cryptographie.

\subsection{Les recommandations des courbes dans la cryptographie }

\subsubsection{Recommandation SafeCurves}

En 2013, Bernstein et Lange ont lancé un site web appelé SafeCurves (\url{https://safecurves.cr.yp.to/index.html})  qui liste les critères pour une analyse structurée de la sécurité des différentes courbes elliptiques. \newline
\newline
Certaines des courbes répertoriées sur ce site sont déployées ou ont été proposées pour déploiement, d’autres ne sont que des exemples destinés à illustrer comment les courbes peuvent ne pas répondre à divers critères de sécurité.\newline
\newline
Le  tableau comparatif ci-dessous (Figure 2) est constitué de 3 colonnes correspondant au nom de la courbe (Curve), à son niveau de sécurité (Safe?) et à ses caractéristiques (Details).

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../ressources/SafeCurves_tableau_1.png}
    \caption{Tableau comparatif SafeCurves}
\end{figure}


Si l’on regarde à la 10ème ligne, on peut constater que la courbe P-256 ne satisfait pas tous les critères et est catégorisée comme “unsafe” (pas sécurisée). \newline
\newline
Un autre tableau est disponible sur SafeCurves afin de regarder plus en détail les exigences voulues notamment au niveau des paramètres de base, de sécurité ECDL (problème du logarithme discret des courbes elliptiques) et enfin ECC (cryptographie sur les courbes elliptiques). 

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{../ressources/SafeCurves_tableau_2.png}
    \caption{Tableau comparatif détaillé SafeCurves}
\end{figure}

Lorsqu’on regarde de nouveau la courbe P-256 (10ème ligne), on note que quatre critères ne sont pas validés :
\begin{enumerate}
    \item Rigidity (Rigidité) : qui concerne la confiance dans les paramètres de la courbe.
    \item Completeness (Exhaustivité) : qui concerne les formules d'addition : \url{https://safecurves.cr.yp.to/complete.html}.
    \item  Indistinguishability from uniform random strings (Indiscernabilité des chaînes aléatoires uniformes) : il s'agit d'un moyen d'encoder un point sur la courbe comme une chaîne aléatoire uniforme.
    \item Ladders (Échelles) : qui fait référence à une manière dont le résultat de la multiplication d’un point \textit{P} par le scalaire \textit{n} (qui se note \textit{nP}) pourraient être calculées.
\end{enumerate}

\subsection{La "porte ouverte" de P-256}

Les paramètres du NIST P-256 sont générés par le hachage de la graine \["C49d3608\quad86e70493\quad6a6678e1\quad139d26b7\quad 819f7e90"\] mais cette graine a été choisie par le NIST sans aucune justification. \newline
Cela a contribué à alimenter des théories quant à d'éventuelles vulnérabilités de la courbe P-256.

\subsubsection{L'attaque de synchronisation du cache dans OpenSSL}

\textit{Nous évoquons ici une faille dans une version d'OpenSSL et non un problème venant de la courbe P-256 elle-même. Il est intéressant de noter} \\

En août 2017, Cesar Pereida Garcia et Billy Bob Brumley dans leur rapport “Constant-time callees with variable-time callers”, exposent la vulnérabilités de P-256 aux attaques de synchronisation du cache dans OpenSSL  1.0.1u. \newline
Cette attaque permet a un utilisateur malveillant disposant d'un accès local de récupérer les clés privées ECDSA P-256 et notamment de récupérer la clé privée de TLS (Protocole de sécurité de la couche de transport) et SSH (Protocole de communication sécurisé), ce qui pouvait être un inconvénient pour DNSSEC étant donné que les logiciels de serveur DNS qui prennent en charge DNSSEC peuvent s’appuyer sur des outils tels que OpenSSL. \\

Diverses techniques d’attaques par canal latéral existent mais dans notre cas, ce sont les attaques par cache (l'attaquant surveille un cache de données qui est partagé entre lui et sa victime) et par synchronisation (l'attaquant mesure la durée d'exécution pendant le chargement des données vers ou depuis un cache ou une mémoire du processeur)  qui vont être utilisées.\newline

La mémoire cache à pour objectif de stocker les informations les plus fréquemment utilisées par les applications et les logiciels d’un ordinateur améliorant ainsi les performances des processeurs. Par ailleurs, au niveau du processeur, le cache est dans un état partagé, c’est-à-dire que lorsque par exemple une routine (fonction ou procédure) publique et une routine secrète s’exécutent en même temps sur une machine, la routine publique pourra voir les adresses mémoires récemment visitées par la routine secrète.\newline

Ainsi, une adresse mémoire peut divulguer des informations secrètes à un attaquant via plusieurs étapes qui peuvent se répéter:

\begin{enumerate}
    	\item L'attaquant expulse la ligne mémoire cible du cache à l’aide du programme malveillant.  
        \item  L'attaquant attend un certain temps pour que la victime ait la possibilité d'accéder à la ligne mémoire. 
        \item  L'attaquant mesure le temps nécessaire pour recharger la ligne mémoire.\newline
        \newline
        La latence mesurée à la dernière étape indique, si oui ou non la ligne mémoire a été accédée par la victime pendant la deuxième étape de l'attaque, c'est-à-dire identifie les succès de cache et les échecs de cache (savoir si les données demandées pour un traitement se trouvent dans la mémoire cache ou non).
\end{enumerate}

Des solutions ont été depuis implémentés dans le NIST 256 pour OpenSSL pour éviter de telles attaques.


\paragraph{Sources \\}

\url{http://essay.utwente.nl/75354/1/DNSSEC%20curves.pdf} \\
\url{https://seclists.org/oss-sec/2017/q1/52} \\
\url{https://en.wikipedia.org/wiki/Timing_attack} \\
\url{https://www.usenix.org/system/files/conference/usenixsecurity17/sec17-garcia.pdf} \\
\url{https://www.josehu.com/technical/2020/06/10/cpu-side-channel.html} \\
\url{https://www.intertrust.com/blog/side-channel-attacks-strategies-and-defenses/} \\

\end{document}

