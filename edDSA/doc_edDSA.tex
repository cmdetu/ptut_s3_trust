\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}

\title{Documentation EdDSA (Edwards-curve Digital Signature Algorithm)}
\date{Décembre 2020}

\begin{document}

\section{EdDSA}
\subsection{Introduction}
EdDSA est un algorithme de signature numérique moderne et sécurisé utilisant les courbes d'Edwards. L’algorithme EdDSA est basé sur l’algorithme de signature de Schnorr et repose sur la difficulté du problème du logarithme discret permettant ainsi de pallier les soucis de sécurité que l’on pouvait trouver dans d’autres fonctions de signature numérique telle que RSA ou encore DSA. \newline

Ed25529 et Ed448 sont des instances  spécifiques de la famille EdDSA et sont décrites dans la RFC 8032.\newline

\begin{figure}[h!]
	\centering
    \includegraphics[scale=0.7]{../ressources/courbe_edward_tordue.png}
    \caption{Courbe d’Edwards tordue d’équation : \(10x^2 + y^2 = 1+ 6x^2y^2\)}
\end{figure}

\subsection{Paramètre de la courbe}

Une courbe d’Edwards tordue admet comme équation : \(ax^2 + y^2 = 1 + dx^2y^2\)

EdDSA admet 11 paramètres pour “fonctionner” : \newline
\begin{enumerate}
    \item un entier \(p\) premier impair (permet de savoir sur quel groupe fini il faut se placer : ici \(GF(p)\) ou plus généralement \(\mathbb{Z}/p\mathbb{Z}\))
    \item un entier \(b\) avec \(2^{b-1} > p\). Il permet de définir la taille de la clé publique (\(b\) bits) ainsi que la signature (\(2b\) bits). Il est recommandé d’avoir un \(b\) multiple de \(8\) pour avoir des octets finis (la taille des clés publiques et des signatures sont sur des nombres entiers d'octet)
    \item un encodage de \(b-1\) bits pour les éléments sur \(GF(p)\)
    \item une fonction de hachage qui ressort \(2b\) bits. Il est préférable d’utiliser une fonction de hachage qui ne crée pas de collision
    \item un entier \(c\) qui prend la valeur 2 ou 3.
    \item un entier $n$ avec $c \leq n < b$. Le scalaire secret de edDSA a exactement $n+1$ bits
    \item un entier $d$ qui n'est pas carré parfait, le plus proche de zéro et qui donne une courbe acceptable
    \item un entier $a$ carré et non-nul. Il est recommandé de prendre $a = 1$ quand $p \mod 4 =1$ et $a = -1$ quand $p \mod 4 = 3$
    \item un point $G \neq (0, 1)$ de la courbe
    \item un premier impair $L$ tel que $LG = O$ (le point à l'infini) et $2^c \times L = \#E$ où $\#E$ est le nombre de points sur la courbe  
    \item une fonction de préhash (pour Ed25119 et Ed448, cette fonction est la fonction identité)\newline
\end{enumerate}


On a donc les paramètres suivant pour la courbe ed25519 : \newline
\begin{itemize}

    \item $p = 2^{255} - 19$
    \item $b = 256$
    \item $255$ bits d’encodage
    \item SHA-512 pour la fonction de hachage
    \item $c = 3$
    \item $n = 254$ 
    \item $d = 37095705934669439343138083508754565189542113879843219016388785533085940283555$
    \item $a = -1$
    \item $G = (15112221349535400772501151409588531511454012693041857206046113283949847762202$,\newline
    $46316835694926478169428394003475163141307993866256225615783033603165251855960)$\\
    \item $L = 2^{252}+27742317777372353535851937790883648493$
    \item $PH(x) = x$
\end{itemize}    

\subsection{Addition de point sur EdDSA}
    
En ce qui concerne l’addition de 2 points sur les courbes d'Edwards, on procède ainsi :
\begin{center}
	\[(x_3, y_3) = (x_1, y_1) + (x_2, y_2)\]
    $x_3 = \frac{(x_1 \times y_2 + x_2 \times y_1)}{(1 + d \times x_1 \times x_2 \times y_1 \times y_2)}$ et
    $y_3 = \frac{(y_1 \times y_2 - a \times x_1 \times x2)}{(1 + d \times x_1 \times x_2 \times y_1 \times y_2 )}$
\end{center}


Pour Ed25519, on représente les coordonnées $(x,y)$ sous la forme d’extended twisted Edwards coordinates (ou coordonnée étendue de la courbe d’Edwards tordue) : 
$(x,y)$ $\rightarrow$ $(X,Y,Z,T)$ avec $x = X / Z$, $y = Y / Z$ et $x \times y = T / Z$. \newline

Ces coordonnées permettent d’utiliser l’algorithme suivant : \newline
    \[A = (Y_1-X_1) \times (Y_2-X_2)\]
    \[B = (Y_1+X_1) \times (Y_2+X_2)\]
    \[C = T_1 \times 2 \times d \times T_2\]
    \[D = Z_1 \times 2 \times Z_2\]
    \[E = B-A\]
    \[F = D-C\]
    \[G = D+C\]
    \[H = B+A\]
    \[X_3 = E \times F\]
    \[Y_3 = G \times H\]
    \[T_3 = E \times H\]
    \[Z_3 = F \times G\]

Pour Ed448, il est recommandé d'utiliser une autre méthode basée sur les coordonnées projectives : \url{https://tools.ietf.org/html/rfc8032#section-5.2.4}

\subsection{Génération de clés EdDSA}

Ed25519 et Ed448 utilisent en même temps de petites clés privées et clés publiques (32 ou 57 octets) et de petites signatures (64 ou 114 octets) avec un niveau de sécurité élevé (respectivement 128 bits ou 224 bits). La sécurité des bits mesure le nombre minimum d’essais requis pour casser le schéma de cryptographique. Ainsi, une sécurité de 128 bits par exemple nécessite au moins $2^{128}$ essais pour casser le schéma.  \newline

La paire de clés EdDSA se compose de : \newline

La \textbf{clé privée} $pk$ est une suite de bits générée aléatoirement : 256 bits pour Ed25519 et 456 bits pour Ed448. Ici, il est évidemment important de faire attention à "l'aléatoire" utilisé. \\

La \textbf{clé publique} $A$ est l'encodage du point $s G$ sur la courbe elliptique. \(s\) est le scalaire secret qui est calculé à partir de la clé privée (son calcul est différent selon que l'on utilise Ed25519 ou Ed448 et se base sur une modification des données obtenues après hachage de la clé privée). \\

Comme pour la clé privée, la clé publique est de $32$ octets pour Ed25519 et $57$ octets pour Ed448 (la fonction de hachage utilisée par Ed448 est SHAKE256 avec une sortie de 114 octets).

\subsection{Compressions}

\subsubsection{Compression (ou encoding)}

\paragraph{Compression d'entier}
Chaque point est codé selon la convention du petit boutiste, c'est-à-dire que chaque point est transformé en une suite d'octets $h$ de 32 octets avec h[0],...,h[31]. L'entier représenté correspond à $h[0] + 2^8 \times h[1] + ... + 2^{248} \times h[31]$.
\paragraph{Compression de point}
Pour chaque point $(x,y)$ vérifiant $0 \leq x,y <p$, on encode d'abord la coordonnée $y$. Ensuite, le bit de poids faible de $x$ est copié sur le bit de poids fort de l'encodage de $y$. \\

Voici un exemple de code pour la compression de point sachant que la fonction \verb|encode| permet d'encoder une valeur :
\begin{minted}{C++}
	std::vector<CryptoPP::byte> tempVector(encode(point.getRealY()));
	if((point.getRealX()) % 2 == 1)
		tempVector.back() |= 128;
	return tempVector;
\end{minted}
\textit{Le bit de poids fort du dernier octet est toujours 0 donc un simple OU logique lorsque \(x\) est impair suffit pour faire la copie de bit.}

\subsection{Signer avec Ed25519}

La signature Ed25519 fonctionne de la manière suivante (Ed448 est très similaire mais demeurent néanmoins quelques différences) \\

\textit{Pour les entiers, c'est le mode de représentation petit-boutiste (little-endian) qui est utilisé} \\

Les données en entrée de cet algorithme sont la clé privée et le message à signer.

\begin{enumerate}
	
	\item Hacher la clé privée : la première moitié de ce haché permet de calculer le scalaire secret \(s\) et la seconde moitié est appelée \(prefix\); à partir du scalaire secret, il est possible de calculer la clé publique \(A\)
	\item Calculer \(r\), interprétation en un entier de \(hash(prefix + message)\) puis calculer \(R\) l'encodage du point \(rG\)
	\item Calculer \(k\), interprétation en un entier de \(hash(R + A + msg)\) puis calculer \(S = (r + k \times s) \mod L\)
	\item La signature est la concaténation de \(R\) et de l'encodage de \(S\)
\end{enumerate}

Ed25519 produit des signatures de 64 octets (114 octets pour Ed448).

\subsection{Vérifier une signature avec Ed25519} 

L’algorithme de vérification de signature Ed25519 prend en entrée un message \(msg\), la clé publique \(A\) du signataire ainsi que la signature.

\begin{enumerate}
	\item La signature est composée de deux parties (concaténation de \(R\) et \(S\) dans la description ci-dessus); il faut décoder ces deux parties pour retrouver le point \(R\) et l'entier \(S\) (qui doit vérifier \(0 \leq S < L\))
	
	\item Décoder la clé publique pour obtenir le point \(A_p\).
	
	\item calculer \(k\), interprétation en un entier de \(hash(R + A + msg)\)
	
	\item Si \(S G = R + k A_p\) est vraie, alors la signature est valide, sinon, elle ne l'est pas.
\end{enumerate}

\[S G = R + kA_p\]
\[(r + k \times s) G = rG + kA_p\]
\[rG + ksG = rG + kA_p\]
\[rG + kA_p = rG + kA_p\]

\subsection{EdDSA vs ECDSA} 

ECDSA et EdDSA ont une forte similitude que ce soit au niveau de la longueur de leur clé ou bien de leur sécurité liée à la complexité du problème du logarithme discret sur lequel ils reposent. Toutefois, EdDSA, utilisant une famille différente de courbes elliptiques (courbe d’Edwards), a un léger avantage en terme de performances face à ECDSA mais cela peut dépendre de l’implémentation. \\

Par ailleurs, contrairement à ECDSA, les signatures EdDSA ne permettent pas de récupérer la clé publique du signataire à partir de la signature et du message. De plus, au lieu de s’appuyer sur un nombre aléatoire pour la valeur nonce tel que le fait ECDSA, EdDSA génère un nonce de manière déterministe sous forme de hachage, ce qui le rend résistant aux collisions. EdDSA sera ainsi d’avantage recommandée pour la plupart des applications modernes. 

\footnote{\url{https://goteleport.com/blog/comparing-ssh-keys/}}
\footnote{\url{https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/digital-signatures/eddsa-and-ed25519.html}}

\subsection{Sources}
 
paramètre de la courbe edDSA : \url{https://tools.ietf.org/html/rfc8032} \\
scalaire secret : \url{https://blog.filippo.io/using-ed25519-keys-for-encryption/} \\

\url{https://crypto.stackexchange.com/questions/56344/what-is-an-elliptic-curve-cofactor}\\
\url{https://crypto.stackexchange.com/questions/51350/what-is-the-relationship-between-p-prime-n-order-and-h-cofactor-of-an-ell?noredirect=1&lq=1}\\
\url{https://cryptologie.net/article/497/eddsa-ed25519-ed25519-ietf-ed25519ph-ed25519ctx-hasheddsa-pureeddsa-wtf/}\\
\url{https://crypto.stackexchange.com/questions/50405/summarize-the-mathematical-problem-at-the-heart-of-breaking-a-curve25519-public/50414#50414}\\
\url{https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/asymmetric-key-ciphers/elliptic-curve-cryptography-ecc.html}\\
\end{document}
