#ifndef IRSAKEY_H
#define IRSAKEY_H

#include <gmpxx.h>

namespace nsCrypto
{
    class RSAKey
    {
        mpz_class m_exponent;
        mpz_class m_n;

    public:
        const mpz_class& get_n() const;
        const mpz_class& get_exponent() const;
        void setExponent(const mpz_class& exp);
        void set_n(const mpz_class& n);
    protected:
        RSAKey(const mpz_class& exp = mpz_class(), const mpz_class& n = mpz_class());
    }; // class RSAKey
} // namespace nsCrypto

#endif // IRSAKEY_H
