TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++14

LIBS += -lcryptopp -pthread

SOURCES += \
        cracker.cpp \
        main.cpp \
        threadsmanager.cpp

HEADERS += \
    cracker.h \
    threadsmanager.h
