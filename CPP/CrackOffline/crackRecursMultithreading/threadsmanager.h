#ifndef THREADSMANAGER_H
#define THREADSMANAGER_H

#include <thread>
#include <mutex>
#include <vector>
#include <string>
#include <algorithm>
#include <atomic>


namespace Password
{
    class ThreadsManager
    {
        struct ManagedThread
        {
            std::unique_ptr<std::thread> m_thread;
            bool m_terminated;

            ManagedThread(std::unique_ptr<std::thread> thread);
        };

        std::mutex m_threadsMutex;
        std::vector<std::unique_ptr<ManagedThread>> m_threads;

        std::atomic_bool m_answerFound;
    public:

        ThreadsManager();

        void setAnswerFoundFlag();
        bool hasAnswerBeenFound();
        void markAsTerminated(const std::thread::id& id);
        void waitForAvailableThread();
        bool aThreadIsAvailable();
        bool addThread(std::unique_ptr<std::thread> t);
        void waitForEndOfAllThreads();

    private:
        bool aThreadIsTerminated();
    };
}

#endif // THREADSMANAGER_H
