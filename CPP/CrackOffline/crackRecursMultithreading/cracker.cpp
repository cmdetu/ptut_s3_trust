#include <iostream>
#include "cryptopp/sha.h"
#include "threadsmanager.h"
#include "cracker.h"

using namespace std;

const std::string Password::Cracker::LOWER_CASE = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

const std::string Password::Cracker::ALPHA = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                              'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

const std::string Password::Cracker::ALPHA_NUM = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                                  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

const std::string Password::Cracker::ALPHA_NUM_SPECIAL = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                                          'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '!', '"', '#', '$', '%', '&', '\'','(', ')', '*', '+', ',', '-', '.', '/',
                                                          ':', ';', '<', '=', '>', '?', '@', '[', '\\',']', '^', '_', '`', '{', '|', '}', '~'};

Password::Cracker::Cracker(const string &hashToCrack)
    : m_hashToCrack(hashToCrack), m_result(""), m_nbTests(0)
{}

void Password::Cracker::setHashToCrack(const std::string& hashToCrack)
{
    m_hashToCrack = hashToCrack;
    m_nbTests = 0;
}

string Password::Cracker::getResult()
{
    return m_result;
}

unsigned long long Password::Cracker::getNbTests()
{
    return m_nbTests;
}

bool Password::Cracker::crack(const size_t maxSize, const string &charSet)
{
    CryptoPP::SHA256 sha256;

    m_nbTests = 0;
    if(charSet.size() == 0 || m_hashToCrack.size() != sha256.DigestSize())
    {
        m_result = "";
        return false;
    }

    std::string hash;
    const std::string emptyString("");
    sha256.Update((const CryptoPP::byte*)emptyString.data(), emptyString.size());
    hash.resize(sha256.DigestSize());
    sha256.Final((CryptoPP::byte*)&hash[0]);
    sha256.Restart();
    ++m_nbTests;

    if(hash == m_hashToCrack)
    {
        m_result = emptyString;
        return true;
    }

    ThreadsManager allThreads;

    for(size_t i(1); i <= maxSize && !allThreads.hasAnswerBeenFound(); ++i)
    {
        if(!allThreads.aThreadIsAvailable())
            allThreads.waitForAvailableThread();

        if(!allThreads.hasAnswerBeenFound())
            allThreads.addThread(make_unique<thread>(&Password::Cracker::crackRecursTrigger, this, cref(charSet), i, ref(allThreads)));
    }
    allThreads.waitForEndOfAllThreads(); // le drapeau de fin a été levé ou les threads sont en cours

    return allThreads.hasAnswerBeenFound();
}

void Password::Cracker::crackRecursTrigger(const string &charSet, size_t size, ThreadsManager& allThreads)
{
    CryptoPP::SHA256 sha256;
    string gen(size, '\0');
    string hash;
    hash.resize(sha256.DigestSize());
    cout << "debut -- thread -> taille : " << size << endl;
    crackRecurs(charSet, gen, 0, hash, allThreads, sha256);
    allThreads.markAsTerminated(this_thread::get_id());
    cout << "fin   -- thread -> taille : " << size << endl;
}

bool Password::Cracker::crackRecurs(const string &charSet, string& gen, size_t pos, string& hash, ThreadsManager& allThreads, CryptoPP::SHA256 &sha256)
{
    if(pos >= gen.size())
    {
        sha256.Update((const CryptoPP::byte*)gen.data(), gen.size());
        sha256.Final((CryptoPP::byte*)&hash[0]);
        sha256.Restart();
        ++m_nbTests;

        if(hash == m_hashToCrack)
        {
            m_result = gen;
            allThreads.setAnswerFoundFlag();
            return true;
        }

        return false;
    }

    for(string::const_iterator it(charSet.begin()); !allThreads.hasAnswerBeenFound() && it != charSet.end(); ++it)
    {
        gen[pos] = *it;
        if(crackRecurs(charSet, gen, pos + 1, hash, allThreads, sha256))
            return true;
    }
    return false;
}
