#ifndef CRACKER_H
#define CRACKER_H

#include <string>
#include <array>
#include "cryptopp/sha.h"

namespace Password
{
    class Cracker
    {
    public:
        static const std::string LOWER_CASE;
        static const std::string ALPHA;
        static const std::string ALPHA_NUM;
        static const std::string ALPHA_NUM_SPECIAL;

    private:
        std::string m_hashToCrack;
        std::string m_result;

        unsigned long long m_nbTests;

        CryptoPP::SHA256 m_sha256;

    public:
        Cracker(const std::string &hashToCrack);
        void setHashToCrack(const std::string& hashToCrack);
        bool crack(const size_t maxSize, const std::string &charSet);
        std::string getResult();
        unsigned long long getNbTests();

    private:
        bool crackRecursV1(const std::string &charSet, std::string& gen,
                                                 size_t pos, std::string& hash, std::string substr);
        bool crackRecursV2(const std::string &charSet, std::string& gen,
                                                 size_t pos, std::string& hash);

    }; // class Cracker
} // namespace Password

#endif // CRACKER_H
