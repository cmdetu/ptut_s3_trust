#include <iostream>
#include <vector>
#include <gmp.h>
#include <gmpxx.h>
#include "SignatureRSA_SHA256/rsasha256signature.h"
#include "RSA/rsa.h"
#include "RSA/rsaprivatekey.h"

using namespace std;

int main()
{
    cout << "Placer les fichiers rfc8017.txt et rfc8017_2.txt dans le répertoire de compilation. Si les fichiers ne sont pas trouvés, une exception est levée." << endl << endl;
    shared_ptr<nsCrypto::RSAPrivateKey> pk;

    // exemple de clé
    mpz_class p("106617547873758523634197219174986053413748553537301940643679262242466831510333");
    mpz_class q("75344300127565738651637443679737652470491109165015816670975989854032773600807");
    nsCrypto::RSAPrivateKey pk2(nsCrypto::RSAPrivateKey::createFromTwoPrimes(p, q));
    pk2.setExponent(mpz_class("6177646766004314467121110014227543959091515565912466495182679882301467446767063909480634657387719282128815644904487203994872109694576970477782343788079873"));

    pk = make_shared<nsCrypto::RSAPrivateKey>(pk2);

    nsSignature::RsaSha256Signature sign("rfc8017.txt");
    sign.setPrivateKey(pk);

    // fichier correct
    vector<uint8_t> signature(sign.sign(true));
    // On utilise le même fichier
    sign.setPublicKey(make_shared<nsCrypto::RSAPublicKey>(sign.getPrivateKey()->getAssociatedPublicKey()));
    cout << boolalpha << sign.check(true, signature) << noboolalpha << endl;

    // fichier modifié
    signature = sign.sign(true);
    sign.setInput("rfc8017_2.txt"); // 1 caractère est différent dans ce fichier
    sign.setPublicKey(make_shared<nsCrypto::RSAPublicKey>(sign.getPrivateKey()->getAssociatedPublicKey()));
    cout << boolalpha << sign.check(true, signature) << noboolalpha << endl;

    return 0;
}
