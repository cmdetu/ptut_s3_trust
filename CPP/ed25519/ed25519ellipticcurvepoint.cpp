#include <exception>

#include "ed25519ellipticcurvepoint.h"
#include "ed25519.h"

using namespace std;

const nsEllipticCurve::Ed25519EllipticCurvePoint &nsEllipticCurve::Ed25519EllipticCurvePoint::getBasePoint()
{
    return nsSignature::Ed25519::_B;
}

const mpz_class &nsEllipticCurve::Ed25519EllipticCurvePoint::getP()
{
    return nsSignature::Ed25519::_p;
}

const mpz_class &nsEllipticCurve::Ed25519EllipticCurvePoint::getL()
{
    return nsSignature::Ed25519::_L;
}

nsEllipticCurve::Ed25519EllipticCurvePoint nsEllipticCurve::Ed25519EllipticCurvePoint::createEd25519Point(const mpz_class &X, const mpz_class &Y, const mpz_class &Z, const mpz_class &T)
{
    mpz_class left((-X * X) + (Y * Y));
    mpz_class right(1 + (nsSignature::Ed25519::_d * X * X * Y * Y));
    if((left - right) % nsSignature::Ed25519::_p != 0)
        throw invalid_argument("this point is not on the curve");
    return Ed25519EllipticCurvePoint(X, Y, Z, T);
}

nsEllipticCurve::Ed25519EllipticCurvePoint::Ed25519EllipticCurvePoint(const mpz_class& X, const mpz_class& Y, const mpz_class& Z, const mpz_class& T)
    : m_X(X), m_Y(Y), m_Z(Z), m_T(T)
{}

const mpz_class& nsEllipticCurve::Ed25519EllipticCurvePoint::getX() const
{
    return m_X;
}

const mpz_class& nsEllipticCurve::Ed25519EllipticCurvePoint::getY() const
{
    return m_Y;
}

const mpz_class& nsEllipticCurve::Ed25519EllipticCurvePoint::getZ() const
{
    return m_Z;
}

const mpz_class& nsEllipticCurve::Ed25519EllipticCurvePoint::getT() const
{
    return m_T;
}

mpz_class nsEllipticCurve::Ed25519EllipticCurvePoint::getRealX() const
{
    mpz_class invZ;
    mpz_invert(invZ.get_mpz_t(), m_Z.get_mpz_t(), nsSignature::Ed25519::_p.get_mpz_t());
    mpz_class x((m_X * invZ) % nsSignature::Ed25519::_p);
    if(x < 0)
        x += nsSignature::Ed25519::_p;
    return x;
}

mpz_class nsEllipticCurve::Ed25519EllipticCurvePoint::getRealY() const
{
    mpz_class invZ;
    mpz_invert(invZ.get_mpz_t(), m_Z.get_mpz_t(), nsSignature::Ed25519::_p.get_mpz_t());
    mpz_class y((m_Y * invZ) % nsSignature::Ed25519::_p);
    if(y < 0)
        y += nsSignature::Ed25519::_p;
    return y;
}

void nsEllipticCurve::Ed25519EllipticCurvePoint::add(const nsEllipticCurve::Ed25519EllipticCurvePoint &other)
{
    mpz_class A(((m_Y - m_X) * (other.m_Y - other.m_X)) % nsSignature::Ed25519::_p);
    mpz_class B(((m_Y + m_X) * (other.m_Y + other.m_X)) % nsSignature::Ed25519::_p);
    mpz_class C((m_T * 2 * nsSignature::Ed25519::_d * other.m_T) % nsSignature::Ed25519::_p);
    mpz_class D((m_Z * 2 * other.m_Z) % nsSignature::Ed25519::_p);

    mpz_class E(B - A);
    mpz_class F(D - C);
    mpz_class G(D + C);
    mpz_class H(B + A);

    m_X = (E * F) % nsSignature::Ed25519::_p;
    m_Y = (G * H) % nsSignature::Ed25519::_p;
    m_T = (E * H) % nsSignature::Ed25519::_p;
    m_Z = (F * G) % nsSignature::Ed25519::_p;

    if(m_X < 0) m_X += nsSignature::Ed25519::_p;
    if(m_Y < 0) m_Y += nsSignature::Ed25519::_p;
    if(m_T < 0) m_T += nsSignature::Ed25519::_p;
    if(m_Z < 0) m_Z += nsSignature::Ed25519::_p;
}

void nsEllipticCurve::Ed25519EllipticCurvePoint::pointMul(mpz_class mul)
{
    nsEllipticCurve::Ed25519EllipticCurvePoint P = *this;
    *this = nsSignature::Ed25519::NEUTRAL_ELEMENT;

    mul %= nsSignature::Ed25519::_L;

    while(mul > 0)
    {
        if(mul % 2 == 1)
            this->add(P);
        P.add(P);
        mul >>= 1 ;
    }
}

bool nsEllipticCurve::Ed25519EllipticCurvePoint::operator ==(const nsEllipticCurve::Ed25519EllipticCurvePoint &other) const
{
    // x = X/Z et y = Y/Z
    // donc (x1 == x2) ^ (y1 == y2)   <=>   (X1/Z1 == X2/Z2) ^ (Y1/Z1 == Y2/Z2)
    // <=>  (X1*Z2 == X2*Z1) ^ (Y1*Z2 == Y2*Z1)

    if(((m_X * other.m_Z) - (other.m_X * m_Z)) % nsSignature::Ed25519::_p != 0)
        return false;

    if(((m_Y * other.m_Z) - (other.m_Y * m_Z)) % nsSignature::Ed25519::_p != 0)
        return false;

    return true;
}
