#include <exception>

#include "ed448ellipticcurvepoint.h"
#include "ed448.h"

using namespace std;

const nsEllipticCurve::Ed448EllipticCurvePoint &nsEllipticCurve::Ed448EllipticCurvePoint::getBasePoint()
{
    return nsSignature::Ed448::_B;
}

const mpz_class &nsEllipticCurve::Ed448EllipticCurvePoint::getP()
{
    return nsSignature::Ed448::_p;
}

const mpz_class &nsEllipticCurve::Ed448EllipticCurvePoint::getL()
{
    return nsSignature::Ed448::_L;
}

nsEllipticCurve::Ed448EllipticCurvePoint nsEllipticCurve::Ed448EllipticCurvePoint::createEd448Point(const mpz_class &X, const mpz_class &Y, const mpz_class &Z)
{
    mpz_class left((X * X) + (Y * Y));
    mpz_class right(1 + (nsSignature::Ed448::_d * X * X * Y * Y));
    if((left - right) % nsSignature::Ed448::_p != 0)
        throw invalid_argument("this point is not on the curve");
    return Ed448EllipticCurvePoint(X, Y, Z);
}

nsEllipticCurve::Ed448EllipticCurvePoint::Ed448EllipticCurvePoint(const mpz_class& X, const mpz_class& Y, const mpz_class& Z)
    : m_X(X), m_Y(Y), m_Z(Z)
{}

const mpz_class& nsEllipticCurve::Ed448EllipticCurvePoint::getX() const
{
    return m_X;
}

const mpz_class& nsEllipticCurve::Ed448EllipticCurvePoint::getY() const
{
    return m_Y;
}

const mpz_class& nsEllipticCurve::Ed448EllipticCurvePoint::getZ() const
{
    return m_Z;
}

mpz_class nsEllipticCurve::Ed448EllipticCurvePoint::getRealX() const
{
    mpz_class invZ;
    mpz_invert(invZ.get_mpz_t(), m_Z.get_mpz_t(), nsSignature::Ed448::_p.get_mpz_t());
    mpz_class x((m_X * invZ) % nsSignature::Ed448::_p);
    if(x < 0)
        x += nsSignature::Ed448::_p;
    return x;
}

mpz_class nsEllipticCurve::Ed448EllipticCurvePoint::getRealY() const
{
    mpz_class invZ;
    mpz_invert(invZ.get_mpz_t(), m_Z.get_mpz_t(), nsSignature::Ed448::_p.get_mpz_t());
    mpz_class y((m_Y * invZ) % nsSignature::Ed448::_p);
    if(y < 0)
        y += nsSignature::Ed448::_p;
    return y;
}

void nsEllipticCurve::Ed448EllipticCurvePoint::add(const nsEllipticCurve::Ed448EllipticCurvePoint &other)
{
    mpz_class A((m_Z * other.m_Z) % nsSignature::Ed448::_p);
    mpz_class B(A * A);
    mpz_class C((m_X * other.m_X) % nsSignature::Ed448::_p);
    mpz_class D((m_Y * other.m_Y) % nsSignature::Ed448::_p);

    mpz_class E((nsSignature::Ed448::_d * C * D) % nsSignature::Ed448::_p);
    mpz_class F(B - E);
    mpz_class G(B + E);
    mpz_class H((m_X + m_Y)*(other.m_X + other.m_Y));

    m_X = ((A * F * (H - C - D)) % nsSignature::Ed448::_p);
    m_Y = ((A * G * (D - C)) % nsSignature::Ed448::_p);
    m_Z = ((F * G) % nsSignature::Ed448::_p);

    if(m_X < 0) m_X += nsSignature::Ed448::_p;
    if(m_Y < 0) m_Y += nsSignature::Ed448::_p;
    if(m_Z < 0) m_Z += nsSignature::Ed448::_p;
}

void nsEllipticCurve::Ed448EllipticCurvePoint::pointMul(mpz_class mul)
{
    mul %= nsSignature::Ed448::_L;

    nsEllipticCurve::Ed448EllipticCurvePoint P = *this;
    *this = nsSignature::Ed448::NEUTRAL_ELEMENT;

    while(mul > 0)
    {
        if(mul % 2 == 1)
            this->add(P);
        P.add(P);
        mul >>= 1 ;
    }
}

bool nsEllipticCurve::Ed448EllipticCurvePoint::operator ==(const nsEllipticCurve::Ed448EllipticCurvePoint &other) const
{
    // x = X/Z et y = Y/Z
    // donc (x1 == x2) ^ (y1 == y2)   <=>   (X1/Z1 == X2/Z2) ^ (Y1/Z1 == Y2/Z2)
    // <=>  (X1*Z2 == X2*Z1) ^ (Y1*Z2 == Y2*Z1)

    if(((m_X * other.m_Z) - (other.m_X * m_Z)) % nsSignature::Ed448::_p != 0)
        return false;

    if(((m_Y * other.m_Z) - (other.m_Y * m_Z)) % nsSignature::Ed448::_p != 0)
        return false;

    return true;
}
