#include "ed25519privatekey.h"
#include "ed25519.h"

nsSignature::nsKeys::Ed25519PrivateKey::Ed25519PrivateKey(const std::string &keyString)
    : nsSignature::nsKeys::EdDSAPrivateKey<Ed25519::KEY_SIZE, Ed25519PublicKey>(keyString)
{}

nsSignature::nsKeys::Ed25519PrivateKey::~Ed25519PrivateKey() {}

std::vector<CryptoPP::byte> nsSignature::nsKeys::Ed25519PrivateKey::hashKey() const
{
    std::vector<CryptoPP::byte> digest(CryptoPP::SHA512::DIGESTSIZE);

    std::vector<CryptoPP::byte> buffer(getKeyAsVector());

    CryptoPP::SHA512 hash;
    hash.CalculateDigest(digest.data(), buffer.data(), buffer.size());
    hash.Restart();

    return digest;
}

// throws if the vector is too small
std::vector<CryptoPP::byte>& nsSignature::nsKeys::Ed25519PrivateKey::pruneData(std::vector<CryptoPP::byte> &data) const
{
    data.at(0) &= 0xf8;
    data.at(31) = (data[31] & 0x7f) | 0x40;

    return data;
}
