#include "rsasha256signature.h"
#include "RSA/RSAException/rsaException.h"

using namespace std;

const std::array<uint8_t, 19> nsSignature::RsaSha256Signature::DERencodingTofTheDigestInfoValue{0x30, 0x31, 0x30, 0x0d, 0x06,
                                                                      0x09, 0x60, 0x86, 0x48, 0x01,
                                                                      0x65, 0x03, 0x04, 0x02, 0x01,
                                                                      0x05, 0x00, 0x04, 0x20};

const unsigned nsSignature::RsaSha256Signature::tLen(nsSignature::RsaSha256Signature::DERencodingTofTheDigestInfoValue.size() + 32 /* 256 bits de SHA-256 */);

nsSignature::RsaSha256Signature::RsaSha256Signature(const string & input /* = std::string() */)
    : m_SHA256(input)
{}

vector<uint8_t> nsSignature::RsaSha256Signature::sign(bool file)
{
    if(m_privateKey == nullptr)
        throw nsCrypto::RSAException("private key not set", nsCrypto::RSAExceptionCode::NoKey);

    m_RSA.setKey(m_privateKey);

    // réutilisation de aBigNumber pour éviter les copies de grands nombres
    mpz_class aBigNumber(m_privateKey->getN());
    unsigned long taille(0); // chercher la taille en octets de n
    while(aBigNumber != 0)
    {
        aBigNumber = aBigNumber >> 8;
        ++taille;
    }

    // peut lancer une exception si le module de chiffrement est trop petit
    // le traitement est laissé à la fonction appelante
    vector<uint8_t> vec(encode(file, taille));

    aBigNumber = convertOS2IP(vec);
    aBigNumber = m_RSA.compute(aBigNumber);
    vec = convertI2OSP(aBigNumber, taille);

    return vec;
}

vector<uint8_t> nsSignature::RsaSha256Signature::encode(bool file, unsigned long emLen)
{
    if(emLen < tLen + 11)
        throw invalid_argument("emLen < tLen + 11");

    vector<uint8_t> result(emLen);

    if(!m_SHA256.isValid())
    {
        if(file)
            m_SHA256.computeFile();
        else
            m_SHA256.computeString();
    }

    array<uint8_t, 32> hashBytes(m_SHA256.getResultBytes());
    result[0] = 0x00;
    result[1] = 0x01;
    for(unsigned i(0); i < emLen - tLen - 3; ++i) // au moins 8 octets (tLen + 11 - tLen - 3 = 11 - 3 = 8)
        result[i + 2] = 0xff;
    result[emLen - tLen] = 0x00; // result[emLen - tLen - 3 + 2 + 1]

    copy(DERencodingTofTheDigestInfoValue.begin(),
         DERencodingTofTheDigestInfoValue.end(),
         result.begin() + long(emLen) - tLen - 3 + 2 + 1);

    copy(hashBytes.rbegin(), hashBytes.rend(), result.rbegin());

    return result;
}

// base 256
mpz_class nsSignature::RsaSha256Signature::convertOS2IP(const std::vector<uint8_t>& v) const
{
    if(v.size() == 0)
        return 0;
    mpz_class result(0);
    for(unsigned i(0); i < v.size(); ++i)
        result = result * 256 + v[i];
    return result;
}

// integer ( > 0 ) to string
vector<uint8_t> nsSignature::RsaSha256Signature::convertI2OSP(mpz_class x, unsigned long length) const
{
    if(x < 0)
        throw invalid_argument("x must be greater or equal to 0");
    mpz_class val;
    mpz_ui_pow_ui(val.get_mpz_t(), 256, length);
    if(x > val)
        throw invalid_argument("integer too large");

    vector<uint8_t> result(length);

    size_t i(result.size() - 1);
    mpz_class mod;
    while(x != 0)
    {
        mod = x % 256;
        result[i] = uint8_t(mod.get_ui()); // fits uint8_t (%256)
        x = x / 256;
        --i;
    }

    return result;
}

bool nsSignature::RsaSha256Signature::check(bool file, const std::vector<uint8_t>& signature)
{
    if(m_publicKey == nullptr)
        throw nsCrypto::RSAException("public key not set", nsCrypto::RSAExceptionCode::NoKey);

    m_RSA.setKey(m_publicKey);

    // réutilisation de aBigNumber pour éviter les copies de grands nombres
    mpz_class aBigNumber(m_publicKey->getN());
    unsigned long taille(0); // chercher la taille en octets de n
    while(aBigNumber != 0)
    {
        aBigNumber = aBigNumber >> 8;
        ++taille;
    }

    if(taille != signature.size())
        return false;

    aBigNumber = convertOS2IP(signature);

    vector<uint8_t> v, vSignature;
    try
    {
        aBigNumber = m_RSA.compute(aBigNumber);
        v = convertI2OSP(aBigNumber, taille);
    }
    catch(const exception& e) // le traitement ne dépend pas du type d'exception
    {
        (void)e;  // éviter warning
        return false;
    }

    try
    {
        vSignature = encode(file, taille);
    }
    catch(const invalid_argument& e) // taille de emLen (= taille) est incorrecte
    {
        (void)e;
        throw nsCrypto::RSAException("Modulus too short", nsCrypto::RSAExceptionCode::ModulusTooShort);
    }


    return equal(v.begin(), v.end(), vSignature.begin()); // v.size() == vSignature.size()
}

void nsSignature::RsaSha256Signature::setPrivateKey(const std::shared_ptr<nsCrypto::RSAPrivateKey>& prK)
{
    m_privateKey = prK;
}

void nsSignature::RsaSha256Signature::setPublicKey(const std::shared_ptr<nsCrypto::RSAPublicKey>& puK)
{
    m_publicKey = puK;
}

std::shared_ptr<nsCrypto::RSAPrivateKey> nsSignature::RsaSha256Signature::getPrivateKey() const
{
    return m_privateKey;
}
std::shared_ptr<nsCrypto::RSAPublicKey> nsSignature::RsaSha256Signature::getPublicKey() const
{
    return m_publicKey;
}

void nsSignature::RsaSha256Signature::setInput(const std::string& input)
{
    m_SHA256.setInputData(input);
}
