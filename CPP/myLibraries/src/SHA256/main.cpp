#include <iostream>
#include <array>
#include <iomanip>

#include "sha256.h"

using namespace std;

int main()
{
    nsHash::SHA256 h("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
    h.computeString();
    cout << h.getResultAsString() << endl;

    nsHash::SHA256 h2("fic.txt");
    h2.computeFile();
    cout << h2.getResultAsString() << endl;

    return 0;
}
