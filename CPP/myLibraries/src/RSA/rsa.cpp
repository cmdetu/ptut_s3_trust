#include "rsa.h"
#include "RSAException/rsaException.h"
#include "RSAException/rsaExceptionCode.h"

using namespace std;

nsCrypto::RSA::RSA(const shared_ptr<nsCrypto::RSAKey>& k /* = nullptr */)
    : m_key(k)
{}

std::shared_ptr<nsCrypto::RSAKey> nsCrypto::RSA::getKey() const
{
    return m_key;
} // getKey

void nsCrypto::RSA::setKey(const shared_ptr<RSAKey>& k)
{
    m_key = k;
} // setKey

mpz_class nsCrypto::RSA::compute(const mpz_class& data) const
{
    if(data >= m_key->getN())
        throw nsCrypto::RSAException("Modulus too short", nsCrypto::RSAExceptionCode::ModulusTooShort);
    mpz_class result;
    mpz_powm(result.get_mpz_t(), data.get_mpz_t(), m_key->getExponent().get_mpz_t(), m_key->getN().get_mpz_t());
    return result;
} // compute
