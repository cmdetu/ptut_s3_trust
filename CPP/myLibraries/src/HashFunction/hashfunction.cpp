#include "hashfunction.h"

nsHash::HashFunction::~HashFunction() {}


nsHash::HashFunction::HashFunction(const std::string &input)
    : m_inputData(input), m_valid(false)
{}

bool nsHash::HashFunction::isValid() const
{
    return m_valid;
}


void nsHash::HashFunction::setInputData(const std::string& inputData)
{
    m_valid = false;
    m_inputData = inputData;
}


const std::string& nsHash::HashFunction::getInputData() const
{
    return m_inputData;
}
