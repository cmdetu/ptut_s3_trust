#ifndef IRSAKEY_H
#define IRSAKEY_H

#include <gmpxx.h>

namespace nsCrypto
{
    class RSAKey
    {
        mpz_class m_exponent;
        mpz_class m_n;

    public:
        const mpz_class& getN() const;
        const mpz_class& getExponent() const;
        void setExponent(const mpz_class& exp);
    protected:
        RSAKey(const mpz_class& exp = mpz_class(), const mpz_class& n = mpz_class());
        void setN(const mpz_class& n);
    }; // class RSAKey
} // namespace nsCrypto

#endif // IRSAKEY_H
