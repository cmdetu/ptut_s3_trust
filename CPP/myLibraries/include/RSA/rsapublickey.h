#ifndef RSAPUBLICKEY_H
#define RSAPUBLICKEY_H

#include <gmpxx.h>
#include "rsakey.h"

namespace nsCrypto
{
    class RSAPrivateKey;
    class RSAPublicKey : public RSAKey
    {
        friend class RSAPrivateKey;

    public:
        RSAPublicKey(const mpz_class& e, const mpz_class& n);
        void setN(const mpz_class& n);
    }; // class RSAPublicKey
} // namespace nsCrypto

#endif // RSAPUBLICKEY_H
